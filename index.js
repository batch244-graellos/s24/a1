let num = 2
let getCube = (num) => num ** 3;

console.log(`The cube of ${num}  is ${getCube(num)}`);


let address = ["258 Washington Ave", "NW", "California", "90011"];
let [street, city, state, zip] = address;
console.log(`I live at ${street}, ${city} ${state} ${zip}`);

let animal = {
    name: "Lolong",
    breed: "saltwater crocodile",
    weight: 1075,
    measurement: "20 ft 3 in"
};

let {name, breed, weight, measurement} = animal;
console.log(`${name} was a ${breed}. He weighted ${weight} kgs with a measurement of ${measurement}.`);

let numbers = [1,2,3,4,5];
numbers.forEach(num => console.log(num));

let reduceNumber = numbers.reduce((accumulator, currentValue) => accumulator + currentValue);
console.log(reduceNumber); //15


class Dog {
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let dog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(dog);


